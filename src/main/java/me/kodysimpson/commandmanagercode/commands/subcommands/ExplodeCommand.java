package me.kodysimpson.commandmanagercode.commands.subcommands;

import me.kodysimpson.commandmanagercode.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ExplodeCommand extends SubCommand{

    @Override
    public String getName() {
        return "explode";
    }

    @Override
    public String getDescription() {
        return "explode someone into smithereens ";
    }

    @Override
    public String getSyntax() {
        return "/prank explode <player>";
    }

    @Override
    public void perform(Player player, String[] args) {

        Player target = Bukkit.getPlayer(args[1]);

        target.playSound(target.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
        target.setHealth(0);
        target.sendMessage("u exploded");

    }

    @Override
    public List<String> tabComplete(Player player, String[] args) {
        if (args.length == 2){
            List<String> playerNames = new ArrayList<>();
            Player[] players = new Player[Bukkit.getServer().getOnlinePlayers().size()];
            Bukkit.getServer().getOnlinePlayers().toArray(players);
            for (int i = 0; i < players.length; i++){
                playerNames.add(players[i].getName());
            }
            return playerNames;
        }
        return null;
    }


}
